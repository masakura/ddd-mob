export class Product {
  constructor (public id: string, public name: string, public price: number) {
  }

  isKatsuhaiTShirts (): boolean {
    return this.name === '克灰袋Tシャツ'
  }
}

export class Cart {
  cartItems: CartItem[] = []
  // TODO 不変オブジェクトを返すようにする
  push (newProduct: Product): void {
    const target = this.cartItems.find(i => i.product.id === newProduct.id)
    if (target != null) {
      // TODO ここを商品の情報に持っていきたい
      if (target.product.isKatsuhaiTShirts() && newProduct.isKatsuhaiTShirts()) {
        throw new Error()
      }
      // TODO 他人のプロパティを書き換えてるの良くないよね
      target.quantity++
    } else {
      this.cartItems.push(new CartItem(newProduct))
    }
  }

  subTotal (): number {
    let sum = 0

    this.cartItems.forEach(p => { sum += p.price() })
    return sum
  }
}

export class CartItem {
  quantity: number
  constructor (public product: Product) {
    this.quantity = 1
  }

  price (): number {
    return this.product.price * this.quantity
  }
}

export const products = {
  克灰袋Tシャツ: new Product('p1', '克灰袋Tシャツ', 2069),
  克灰袋: new Product('p2', '克灰袋', 100),
  克灰袋スウェット: new Product('p3', '克灰袋スウェット', 3901)
}

describe('商品をカートに投入できる', function () {
  it('1つの商品をカートに投入できる', function () {
    const cart = new Cart()

    cart.push(products.克灰袋Tシャツ)

    expect(cart.cartItems[0].product.name).toEqual(products.克灰袋Tシャツ.name)
  })

  it('１つ以上の別の商品をカートに追加できる', function () {
    const cart = new Cart()
    cart.push(products.克灰袋Tシャツ)
    cart.push(products.克灰袋)

    expect(cart.cartItems[0].product.name).toEqual(products.克灰袋Tシャツ.name)
    expect(cart.cartItems[1].product.name).toEqual(products.克灰袋.name)
  })

  it('克灰袋Tシャツは２つ以上追加できない', function () {
    const cart = new Cart()
    cart.push(products.克灰袋Tシャツ)

    expect(() => cart.push(products.克灰袋Tシャツ)).toThrow()
  })

  it('カート内の小計を計算する', function () {
    const cart = new Cart()
    cart.push(products.克灰袋Tシャツ)
    cart.push(products.克灰袋)

    expect(cart.subTotal()).toEqual(products.克灰袋Tシャツ.price + products.克灰袋.price)
  })

  it('同じ商品を２つ以上追加して個数x金額を計算', () => {
    const cart = new Cart()
    cart.push(products.克灰袋スウェット)
    cart.push(products.克灰袋スウェット)

    expect(cart.subTotal()).toEqual(products.克灰袋スウェット.price * 2)
    const item = cart.cartItems.find(x => x.product.id === products.克灰袋スウェット.id)
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(item!.price()).toEqual(products.克灰袋スウェット.price * 2)
  })
})

class CartRepository {
  private readonly cartProducts: any[] = []

  save (cart: Cart): void {
    for (const item of cart.cartItems) {
      this.cartProducts.push({
        id: item.product.id,
        name: item.product.name,
        price: item.product.price
      })
    }
  }

  load (): Cart {
    const cart = new Cart()
    this.cartProducts.forEach(p => {
      const product = new Product(p.id, p.name, p.price)
      cart.push(product)
    })
    return cart
  }
}
describe('永続化とか', function () {
  it('カートの内容を保存→読み込み料金が一致する', function () {
    const cart = new Cart()
    cart.push(products.克灰袋)

    const cartRepository = new CartRepository()
    cartRepository.save(cart)

    const loadCart = cartRepository.load()
    expect(loadCart.subTotal()).toEqual(cart.subTotal())
  })
})
