import { Cart, CartItem, products } from './main.test'

interface DeliveryDestination {
  readonly name: string
  readonly address: string
}

class Order {
  readonly orderItems: OrderItems

  constructor (cart: Cart, readonly deliveryDestination?: DeliveryDestination) {
    this.orderItems = new OrderItems(cart.cartItems)
  }

  subtotalPrice (): number {
    return this.orderItems.totalPrice()
  }

  totalPrice (): number {
    return this.subtotalPrice() + this.shippingFee()
  }

  private static readonly shippingFeeFreeBorder: number = 5000

  shippingFee (): number {
    if (this.isFreeShipping()) {
      return 0
    }
    return 500
  }

  private isFreeShipping (): boolean {
    return this.subtotalPrice() >= Order.shippingFeeFreeBorder
  }
}

class OrderItem {
  constructor (cartItem: CartItem) {
    this.quantity = cartItem.quantity
    this.price = cartItem.price()
  }

  readonly quantity: number
  readonly price: number
}

class OrderItems {
  readonly orderItems: readonly OrderItem[]
  constructor (cartItems: readonly CartItem[]) {
    this.orderItems = cartItems.map(c => new OrderItem(c))
  }

  totalQuantity (): number {
    let sum = 0
    for (const orderItem of this.orderItems) {
      sum += orderItem.quantity
    }
    return sum
  }

  totalPrice (): number {
    let sum = 0
    for (const orderItem of this.orderItems) {
      sum += orderItem.price
    }
    return sum
  }
}

describe('Order', function () {
  it('カートに入っている商品から明細を作成する', () => {
    const cart = new Cart()
    cart.push(products.克灰袋)
    cart.push(products.克灰袋)
    cart.push(products.克灰袋Tシャツ)

    const order = new Order(cart)

    expect(order.orderItems.totalQuantity()).toEqual(3)
  })

  it('カートから商品の小計を計算する', () => {
    const cart = new Cart()
    cart.push(products.克灰袋)
    cart.push(products.克灰袋)
    cart.push(products.克灰袋Tシャツ)

    const order = new Order(cart)

    expect(order.subtotalPrice()).toEqual(100 + 100 + 2069)
  })

  it('送料は基本 500 円', () => {
    const cart = new Cart()
    cart.push(products.克灰袋)

    const order = new Order(cart)

    expect(order.shippingFee()).toEqual(500)
  })

  it('カート内の合計が5000円以上になると送料が0円になる', () => {
    const cart = new Cart()
    cart.push(products.克灰袋Tシャツ)
    cart.push(products.克灰袋スウェット)

    const order = new Order(cart)

    expect(order.shippingFee()).toEqual(0)
  })

  it('合計は小計と送料の合計である', () => {
    const cart = new Cart()
    cart.push(products.克灰袋)

    const order = new Order(cart)

    expect(order.totalPrice()).toEqual(products.克灰袋.price + 500)
  })

  it('小計が5000円を超える場合には送料が支払い総額に含まれない', () => {
    const cart = new Cart()
    cart.push(products.克灰袋Tシャツ)
    cart.push(products.克灰袋スウェット)

    const order = new Order(cart)

    expect(order.totalPrice()).toEqual(order.subtotalPrice())
  })

  it('配送先を設定する', () => {
    const cart = new Cart()
    const order = new Order(cart, { name: '山田 太郎', address: '鹿児島市のどこか' })

    expect(order.deliveryDestination).toEqual({
      name: '山田 太郎',
      address: '鹿児島市のどこか'
    })
  })
})
